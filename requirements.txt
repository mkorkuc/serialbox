django
djangorestframework
decorator
djangorestframework-csv
# djangorestframework-xml
Markdown==2.6.2
psycopg2
python-dotenv
six
django-guardian
django-model-utils
quartet_templates
quartet_capture
quartet_output
gs123
jinja2
-e git://github.com/rmagee/django-rest-framework-xml.git#egg=djangorestframework-xml
epcpyyes
